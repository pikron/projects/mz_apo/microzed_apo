{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 1 1 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref U? }
    {PPP 1 }
    {PinL
      ? 13
      ? 17
      ? 18
      ? 11
      ? 12
      ? 14
      ? 15
      ? 20
      ? 19
      ? 10
      ? 16
      ? 4
      ? 5
      ? 3
      ? 6
      ? 2
      ? 1
      ? 21
      ? 24
      ? 23
      ? 22
      ? 7
      ? 9
      ? 8
    }
    {Pkgs
      SMD1 QFN24-0.5
    }
    {Lock 0 }
   {PPin : None
    {Pos -30 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 8 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text DACDAT }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 9 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text DACLRC }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 7 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text BCLK }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 22 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text CSB }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 23 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text SDIN }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 24 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text SCLK }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 21 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text MODE }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 1 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text XTI }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 2 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text XTO }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 360 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 6 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 360 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text CLKOUT }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 420 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 3 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 420 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text DCVDD }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 450 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 5 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 450 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text DBVDD }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 480 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 4 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 480 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text DGND }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos 270 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 16 }
    {Name SHORT }
   }
   {PText : None
    {Pos 230 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text AVDD }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 10 }
    {Name SHORT }
   }
   {PText : None
    {Pos 230 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text HPVDD }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 19 }
    {Name SHORT }
   }
   {PText : None
    {Pos 230 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RIN }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 20 }
    {Name SHORT }
   }
   {PText : None
    {Pos 230 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text LIN }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 15 }
    {Name SHORT }
   }
   {PText : None
    {Pos 230 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text ROUT }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 14 }
    {Name SHORT }
   }
   {PText : None
    {Pos 230 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text LOUT }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 12 }
    {Name SHORT }
   }
   {PText : None
    {Pos 230 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RHPOUT }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 11 }
    {Name SHORT }
   }
   {PText : None
    {Pos 230 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text LHPOUT }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 390 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 18 }
    {Name SHORT }
   }
   {PText : None
    {Pos 230 390 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text VMID }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 450 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 17 }
    {Name SHORT }
   }
   {PText : None
    {Pos 230 450 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text AGND }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 480 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 13 }
    {Name SHORT }
   }
   {PText : None
    {Pos 230 480 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text HPGND }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 240 510 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Rect 0 0 240 510 }
   }
  }
