{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 1 1 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref U? }
    {PPP 2 }
    {PinL
      GND 11 11
      VCC 4 4
      ? 2 6
      ? 13 8
      ? 3 10
      ? 1 5
      ? 12 9
      ? 14 7
    }
    {Pkgs
      SMD2 SSOP14-0.65
      SMD1 SO14
    }
    {Lock 0 }
   {PPin : None
    {Pos -30 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 14 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text J }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos 150 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 12 }
    {Name SHORT }
   }
   {PText : None
    {Pos 110 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text Q }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos -90 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 1 }
    {Name DOTCLK }
   }
   {PText : None
    {Pos 10 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text CLK }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 3 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text K }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos 150 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 13 }
    {Name SHORT }
   }
   {PText : None
    {Pos 110 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text Q\ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 60 180 }
    {Ly  PrBody }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 2 }
    {Name SHORT }
   }
   {PText : None
    {Pos 60 140 }
    {Ly  PrBody }
    {Rot 3 }
    {Mir 0 }
    {Wd 1 }
    {Text R\ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : Power
    {Pos 60 -90 }
    {Ly  PrBody }
    {Rot 1 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 4 }
    {Name NORM }
   }
   {PPin : Power
    {Pos 60 240 }
    {Ly  PrBody }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 11 }
    {Name NORM }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 120 150 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Rect 0 0 120 150 }
   }
  }
