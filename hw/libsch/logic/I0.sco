{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 1 1 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref  }
    {PPP 1 }
    {PinL
      ? 0
    }
    {Pkgs
    }
    {Lock 0 }
   {PPin : None
    {Pos -90 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 0 }
    {Name NORM }
   }
   {PText : None
    {Pos 10 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text 0.0 +0.3 LINE +0.0 +0.3 +0.0 +3.7 LINE +0.0 +3.7 +2.2 +3.7 LINE +1.0 +1.0 +1.8 +1.0 LINE +1.0 +1.0 +1.0 +3.0 LINE +0.7 +3.0 +1.5 +3.0 LINE +1.5 +1.0 +1.5 +3.0 ARC +2.3 +2.0 +0.0 -1.7 +1.7 +0.0 +1.7 ARC +2.3 +2.0 +1.7 +0.0 +0.0 +1.7 +1.7 END  }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 0 0 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Rect 0 0 0 0 }
   }
  }
