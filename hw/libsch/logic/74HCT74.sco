{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 1 1 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref U? }
    {PPP 2 }
    {PinL
      GND 7 7
      VCC 14 14
      ? 1 13
      ? 4 10
      ? 6 8
      ? 3 11
      ? 5 9
      ? 2 12
    }
    {Pkgs
      SMD2 SSOP14-0.65
      SMD1 SO14
    }
    {Lock 0 }
   {PPin : None
    {Pos -30 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 2 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text D }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos 150 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 5 }
    {Name SHORT }
   }
   {PText : None
    {Pos 110 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text Q }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos -90 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 3 }
    {Name CLK }
   }
   {PText : None
    {Pos 10 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text CLK }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos 150 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 6 }
    {Name SHORT }
   }
   {PText : None
    {Pos 110 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text Q\ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 60 -30 }
    {Ly  PrBody }
    {Rot 1 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 4 }
    {Name SHORT }
   }
   {PText : None
    {Pos 60 10 }
    {Ly  PrBody }
    {Rot 3 }
    {Mir 0 }
    {Wd 1 }
    {Text S\ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 60 210 }
    {Ly  PrBody }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 1 }
    {Name SHORT }
   }
   {PText : None
    {Pos 60 170 }
    {Ly  PrBody }
    {Rot 3 }
    {Mir 0 }
    {Wd 1 }
    {Text R\ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : Power
    {Pos 90 -90 }
    {Ly  PrBody }
    {Rot 1 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 14 }
    {Name NORM }
   }
   {PPin : Power
    {Pos 90 270 }
    {Ly  PrBody }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 7 }
    {Name NORM }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 120 180 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Rect 0 0 120 180 }
   }
  }
