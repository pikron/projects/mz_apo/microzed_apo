{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 1 1 1 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody PINNUM SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref  }
    {PPP 1 }
    {PinL
      ? 16
      ? 15
      ? 1
      ? 2
      ? 3
      ? 4
      ? 5
      ? 6
      ? 7
      ? 8
      ? 9
      ? 10
      ? 11
      ? 12
      ? 13
      ? 14
    }
    {Pkgs
    }
    {Lock 0 }
   {PPin : None
    {Pos 30 330 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 14 }
    {Name NORM }
   }
   {PPin : None
    {Pos 60 330 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 13 }
    {Name NORM }
   }
   {PPin : None
    {Pos 90 330 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 12 }
    {Name NORM }
   }
   {PPin : None
    {Pos 120 330 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 11 }
    {Name NORM }
   }
   {PPin : None
    {Pos 150 330 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 10 }
    {Name NORM }
   }
   {PPin : None
    {Pos 180 330 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 9 }
    {Name NORM }
   }
   {PPin : None
    {Pos 210 330 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 8 }
    {Name NORM }
   }
   {PPin : None
    {Pos 240 330 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 7 }
    {Name NORM }
   }
   {PPin : None
    {Pos 300 330 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 6 }
    {Name NORM }
   }
   {PPin : None
    {Pos 330 330 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 5 }
    {Name NORM }
   }
   {PPin : None
    {Pos 360 330 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 4 }
    {Name NORM }
   }
   {PPin : None
    {Pos 450 330 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 3 }
    {Name NORM }
   }
   {PPin : None
    {Pos 480 330 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 2 }
    {Name NORM }
   }
   {PPin : None
    {Pos 510 330 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 1 }
    {Name NORM }
   }
   {PPin : None
    {Pos -30 150 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 15 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 90 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 16 }
    {Name SHORT }
   }
   {PLine : None
    {Pos 540 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -540 0 }
   }
   {PLine : None
    {Pos 0 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 -240 }
   }
   {PLine : None
    {Pos 0 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 540 0 }
   }
   {PLine : None
    {Pos 540 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 240 }
   }
   {PLine : None
    {Pos 510 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -480 0 }
   }
   {PLine : None
    {Pos 30 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 90 }
   }
   {PLine : None
    {Pos 30 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 480 0 }
   }
   {PLine : None
    {Pos 510 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 -90 }
   }
   {PText : None
    {Pos 90 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text LCD_16x2 }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 90 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text DATA }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 30 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text 7 }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 60 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text 6 }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 90 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text 5 }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 120 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text 4 }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 150 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text 3 }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 180 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text 2 }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 210 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text 1 }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 240 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text 0 }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 300 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text E }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 330 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text R }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 330 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text W }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 360 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text R }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 360 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text S }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 330 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text _ }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 330 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text / }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 450 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text V }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 450 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text O }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 480 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text + }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 480 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text 5 }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 480 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text V }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 510 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text G }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 510 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text N }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PText : None
    {Pos 510 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text D }
    {Font "Courier" 20 0 0 ISO }
    {Alig 0 2 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 540 240 }
   }
  }
