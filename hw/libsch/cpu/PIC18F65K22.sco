{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 1 1 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref U? }
    {PPP 1 }
    {PinL
      ? 65
      GND 56
      GND 41
      GND 25
      GND 9
      ? 20
      ? 18
      ? 10
      ? 57
      ? 38
      ? 26
      ? 19
      ? 8
      ? 7
      ? 6
      ? 5
      ? 4
      ? 3
      ? 11
      ? 12
      ? 13
      ? 14
      ? 15
      ? 16
      ? 17
      ? 59
      ? 60
      ? 61
      ? 62
      ? 63
      ? 64
      ? 1
      ? 2
      ? 49
      ? 50
      ? 51
      ? 52
      ? 53
      ? 54
      ? 55
      ? 58
      ? 32
      ? 31
      ? 36
      ? 35
      ? 34
      ? 33
      ? 29
      ? 30
      ? 37
      ? 42
      ? 43
      ? 44
      ? 45
      ? 46
      ? 47
      ? 48
      ? 39
      ? 40
      ? 27
      ? 28
      ? 21
      ? 22
      ? 23
      ? 24
    }
    {Pkgs
      SMD2 QFN64-0.5
      SMD1 TQFP64-0.5
    }
    {Lock 0 }
   {PPin : None
    {Pos -30 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 24 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RA0/AN0/ULPWU }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 23 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RA1/AN1 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 22 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RA2/AN2/VREF- }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 21 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RA3/AN3/VREF+ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 28 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RA4/T0CKI }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 27 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RA5/AN4/T1CKI/T3G/HLVDIN }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 40 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text OSC2/CLKO/RA6 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 39 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text OSC1/CLKI/RA7 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 48 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RB0/INT0/FLT0 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 47 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RB1/INT1 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 360 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 46 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 360 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RB2/INT2/CTED1 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 390 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 45 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 390 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RB3/INT3/CTED2/ECCP2/PA2 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 420 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 44 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 420 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RB4/KBI0 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 450 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 43 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 450 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RB5/KBI1/T3CKI/T1G }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 480 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 42 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 480 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RB6/KBI2/PGC }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 510 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 37 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 510 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RB7/KBI3/PGD }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 570 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 30 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 570 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RC0/SOSCO/SCLKI }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 600 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 29 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 600 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RC1/SOSCI/ECCP2/P2A }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 630 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 33 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 630 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RC2/ECCP1/P1A }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 660 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 34 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 660 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RC3/SCK1/SCL1 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 690 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 35 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 690 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RC4/SDI1/SDA1 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 720 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 36 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 720 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RC5/SDO1 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 750 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 31 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 750 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RC6/TX1/CK1 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 780 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 32 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 780 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RC7/RX1/DT1 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 840 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 58 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 840 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RD0/PSP0/CTPLS }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 870 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 55 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 870 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RD1/PSP1/T5CKI/T7G }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 900 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 54 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 900 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RD2/PSP2 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 930 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 53 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 930 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RD3/PSP3 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 960 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 52 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 960 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RD4/PSP4/SDO2 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 990 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 51 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 990 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RD5/PSP5/SDI2/SDA2 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 1020 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 50 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 1020 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RD6/PSP6/SCK2/SCL2 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 1050 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 49 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 1050 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RD7/PSP7/SS2 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos 780 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 2 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RE0/RD/P2D }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 1 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RE1/WR/P2C }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 360 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 64 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 360 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RE2/CS/P2B/CCP10 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 390 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 63 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 390 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RE3/P3C/CCP9/REF0 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 420 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 62 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 420 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RE4/P3B/CCP8 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 450 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 61 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 450 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RE5/P1C/CCP7 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 480 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 60 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 480 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RE6/P1B/CCP6 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 510 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 59 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 510 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RE7/ECCP2/P2A }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 570 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 17 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 570 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RF1/AN6/C2OUT/CTDIN }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 600 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 16 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 600 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RF2/AN7/C1OUT }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 630 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 15 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 630 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RF3/AN8/C2INB/CTMUI }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 660 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 14 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 660 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RF4/AN9/C2INA }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 690 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 13 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 690 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RF5/AN10/CVREF/C1INB }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 720 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 12 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 720 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RF6/AN11/C1INA }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 750 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 11 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 750 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RF7/AN5/SS1 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 810 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 3 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 810 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RG0/ECCP3/P3A }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 840 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 4 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 840 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RG1/TX2/CK2/AN19/C3OUT }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 870 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 5 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 870 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RG2/RX2/DT2/AN18/C3INA }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 900 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 6 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 900 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RG3/CCP4/AN17/P3D/C3INB }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 930 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 7 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 930 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text MCLR/RG5 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 960 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 8 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 960 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RG4/T7CKI/T5G/CCP5/AN16/P1D/C3INC }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 19 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text AVDD }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 26 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text VDD }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 38 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text VDD }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 57 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text VDD }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 10 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text VDDCORE/VCAP }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 18 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text ENVREG }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 780 1020 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 20 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 1020 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text AVSS }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : Power
    {Pos 210 1110 }
    {Ly  PrBody }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 9 }
    {Name SHORT }
   }
   {PPin : Power
    {Pos 240 1110 }
    {Ly  PrBody }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 25 }
    {Name SHORT }
   }
   {PPin : Power
    {Pos 270 1110 }
    {Ly  PrBody }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 41 }
    {Name SHORT }
   }
   {PPin : Power
    {Pos 300 1110 }
    {Ly  PrBody }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 56 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 780 1050 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 65 }
    {Name SHORT }
   }
   {PText : None
    {Pos 740 1050 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text GND }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 750 1080 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Rect 0 0 750 1080 }
   }
  }
