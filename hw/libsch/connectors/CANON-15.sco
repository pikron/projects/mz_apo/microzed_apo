{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 1 1 0 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody PINNUM SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref CN? }
    {PPP 1 }
    {PinL
      ? 1
      ? 9
      ? 2
      ? 10
      ? 3
      ? 11
      ? 4
      ? 12
      ? 5
      ? 13
      ? 6
      ? 14
      ? 7
      ? 15
      ? 8
    }
    {Pkgs
    }
    {Lock 0 }
   {PPin : None
    {Pos -30 30 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 8 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 60 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 15 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 90 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 7 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 120 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 14 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 150 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 6 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 180 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 13 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 210 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 5 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 240 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 12 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 270 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 4 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 300 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 11 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 330 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 3 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 360 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 10 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 390 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 2 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 420 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 9 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 450 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 1 }
    {Name SHORT }
   }
   {PLine : None
    {Pos 0 9 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 9 -9 }
   }
   {PLine : None
    {Pos 33 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -24 0 }
   }
   {PLine : None
    {Pos 33 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 57 33 }
   }
   {PLine : None
    {Pos 90 447 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 -414 }
   }
   {PLine : None
    {Pos 90 447 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -57 33 }
   }
   {PLine : None
    {Pos 9 480 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 24 0 }
   }
   {PLine : None
    {Pos 9 480 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -9 -9 }
   }
   {PLine : None
    {Pos 0 9 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 462 }
   }
   {PLine : None
    {Pos 0 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 21 0 }
   }
   {PCir : None
    {Pos 27 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 57 0 }
   }
   {PCir : None
    {Pos 69 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 21 0 }
   }
   {PCir : None
    {Pos 27 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 57 0 }
   }
   {PCir : None
    {Pos 69 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 21 0 }
   }
   {PCir : None
    {Pos 27 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 57 0 }
   }
   {PCir : None
    {Pos 69 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 21 0 }
   }
   {PCir : None
    {Pos 27 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 57 0 }
   }
   {PCir : None
    {Pos 69 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 270 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 21 0 }
   }
   {PCir : None
    {Pos 27 270 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 57 0 }
   }
   {PCir : None
    {Pos 69 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 21 0 }
   }
   {PCir : None
    {Pos 27 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 360 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 57 0 }
   }
   {PCir : None
    {Pos 69 360 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 390 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 21 0 }
   }
   {PCir : None
    {Pos 27 390 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 420 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 57 0 }
   }
   {PCir : None
    {Pos 69 420 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 450 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 21 0 }
   }
   {PCir : None
    {Pos 27 450 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 90 480 }
   }
  }
