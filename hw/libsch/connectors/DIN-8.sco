{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 1 1 0 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody PINNUM SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref CN? }
    {PPP 1 }
    {PinL
      ? 7
      ? 6
      ? 1
      ? 4
      ? 2
      ? 5
      ? 3
      ? 8
    }
    {Pkgs
    }
    {Lock 0 }
   {PPin : None
    {Pos 0 -30 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 8 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 60 -30 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 3 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 120 -30 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 5 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 150 60 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 2 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 120 150 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 4 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 60 150 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 1 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 0 150 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 6 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 30 -30 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 7 }
    {Name SHORT }
   }
   {PLine : None
    {Pos 0 72 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 12 0 }
   }
   {PLine : None
    {Pos 12 72 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 -24 }
   }
   {PLine : None
    {Pos 12 48 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -12 0 }
   }
   {PLine : None
    {Pos 30 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 21 }
   }
   {PLine : None
    {Pos 60 9 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 -9 }
   }
   {PLine : None
    {Pos 120 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -24 24 }
   }
   {PLine : None
    {Pos 111 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 9 0 }
   }
   {PLine : None
    {Pos 120 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -24 -24 }
   }
   {PLine : None
    {Pos 60 111 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 9 }
   }
   {PLine : None
    {Pos 0 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 24 -24 }
   }
   {PLine : None
    {Pos 0 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 27 }
   }
   {PLine : None
    {Pos 0 27 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 36 36 }
   }
   {PLine : None
    {Pos 36 63 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 12 0 }
   }
   {PCir : None
    {Pos 60 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -60 -60 120 120 }
    {Angle 3060 1259 }
   }
   {PCir : None
    {Pos 60 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -60 -60 120 120 }
    {Angle 4319 1440 }
   }
   {PCir : None
    {Pos 60 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -60 -60 120 120 }
    {Angle 0 1440 }
   }
   {PCir : None
    {Pos 60 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -60 -60 120 120 }
    {Angle 1440 1259 }
   }
   {PCir : None
    {Pos 99 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -12 -12 24 24 }
   }
   {PCir : None
    {Pos 60 21 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -12 -12 24 24 }
   }
   {PCir : None
    {Pos 60 99 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -12 -12 24 24 }
   }
   {PCir : None
    {Pos 87 87 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -12 -12 24 24 }
   }
   {PCir : None
    {Pos 87 33 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -12 -12 24 24 }
   }
   {PCir : None
    {Pos 33 33 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -12 -12 24 24 }
   }
   {PCir : None
    {Pos 33 87 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -12 -12 24 24 }
   }
   {PCir : None
    {Pos 60 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -12 -12 24 24 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 120 120 }
   }
  }
