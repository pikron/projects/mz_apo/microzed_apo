{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 1 1 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref CN? }
    {PPP 1 }
    {PinL
      ? 32
      ? 31
      ? 30
      ? 29
      ? 28
      ? 27
      ? 26
      ? 25
      ? 24
      ? 23
      ? 22
      ? 21
      ? 20
      ? 19
      ? 18
      ? 17
      ? 16
      ? 15
      ? 14
      ? 13
      ? 12
      ? 11
      ? 10
      ? 9
      ? 8
      ? 7
      ? 6
      ? 5
      ? 4
      ? 3
      ? 2
      ? 1
    }
    {Pkgs
    }
    {Lock 0 }
   {PPin : None
    {Pos 270 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 1 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text GND }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 2 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text RES DRV }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 3 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text +5V }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 4 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text IRQ9 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 5 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text -5V }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 6 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text DRQ2 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 7 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text -12V }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 8 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text S\R\D\Y\ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 270 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 9 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 270 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text +12V }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 10 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text GND }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 11 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text S\M\E\M\W\ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 360 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 12 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 360 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text S\M\E\M\R\ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 390 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 13 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 390 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text I\O\W\ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 420 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 14 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 420 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text I\O\R\ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 450 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 15 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 450 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text D\A\C\K\3\ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 480 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 16 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 480 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text DRQ3 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 510 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 17 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 510 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text D\A\C\K\1\ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 540 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 18 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 540 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text DRQ1 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 570 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 19 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 570 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text R\E\F\R\E\S\H\ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 600 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 20 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 600 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text BCLK }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 630 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 21 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 630 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text IRQ7 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 660 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 22 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 660 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text IRQ6 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 690 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 23 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 690 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text IRQ5 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 720 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 24 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 720 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text IRQ4 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 750 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 25 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 750 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text IRQ3 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 780 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 26 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 780 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text D\A\C\K\2\ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 810 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 27 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 810 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text T/C }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 840 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 28 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 840 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text BALE }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 870 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 29 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 870 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text +5V }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 900 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 30 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 900 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text OSC }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 930 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 31 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 930 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text GND }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PPin : None
    {Pos 270 960 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 32 }
    {Name NORM }
   }
   {PText : None
    {Pos 170 960 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text GND }
    {Font "Courier" 19 0 0 ISO }
    {Alig 2 1 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 180 990 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Rect 0 0 180 990 }
   }
  }
