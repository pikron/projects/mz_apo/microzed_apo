{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 1 1 0 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody PINNUM SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref CN? }
    {PPP 1 }
    {PinL
      ? 1
      ? 6
      ? 2
      ? 7
      ? 3
      ? 8
      ? 4
      ? 9
      ? 5
    }
    {Pkgs
    }
    {Lock 0 }
   {PPin : None
    {Pos -30 30 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 5 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 60 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 9 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 90 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 4 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 120 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 8 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 150 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 3 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 180 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 7 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 210 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 2 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 240 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 6 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 270 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 1 }
    {Name SHORT }
   }
   {PLine : None
    {Pos 0 9 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 9 -9 }
   }
   {PLine : None
    {Pos 33 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -24 0 }
   }
   {PLine : None
    {Pos 33 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 57 33 }
   }
   {PLine : None
    {Pos 90 267 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 -234 }
   }
   {PLine : None
    {Pos 90 267 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -57 33 }
   }
   {PLine : None
    {Pos 9 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 24 0 }
   }
   {PLine : None
    {Pos 9 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -9 -9 }
   }
   {PLine : None
    {Pos 0 9 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 282 }
   }
   {PLine : None
    {Pos 0 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 21 0 }
   }
   {PCir : None
    {Pos 27 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 57 0 }
   }
   {PCir : None
    {Pos 69 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 21 0 }
   }
   {PCir : None
    {Pos 27 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 57 0 }
   }
   {PCir : None
    {Pos 69 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 21 0 }
   }
   {PCir : None
    {Pos 27 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 57 0 }
   }
   {PCir : None
    {Pos 69 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 21 0 }
   }
   {PCir : None
    {Pos 27 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 57 0 }
   }
   {PCir : None
    {Pos 69 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PLine : None
    {Pos 0 270 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 21 0 }
   }
   {PCir : None
    {Pos 27 270 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 90 300 }
   }
  }
