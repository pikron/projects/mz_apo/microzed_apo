{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 1 1 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref CN? }
    {PPP 1 }
    {PinL
      GND 12
      GND 11
      ? 10
      ? 9
      ? 8
      ? 7
      ? 6
      ? 5
      ? 4
      ? 3
      ? 2
      ? 1
    }
    {Pkgs
    }
    {Lock 0 }
   {PPin : None
    {Pos -30 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 1 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text NC }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 2 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text C\S\ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 3 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text DI(MOSI) }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 4 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text VCC }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 5 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text CLK }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 6 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 180 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text GND }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 7 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text DO(MISO) }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 8 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 240 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text IRQ }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 9 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 300 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text CD1 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : None
    {Pos -30 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 10 }
    {Name SHORT }
   }
   {PText : None
    {Pos 10 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Text CD2 }
    {Font "Courier" 19 0 0 ISO }
    {Alig 0 1 }
   }
   {PPin : Power
    {Pos 30 390 }
    {Ly  PrBody }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 11 }
    {Name SHORT }
   }
   {PPin : Power
    {Pos 60 390 }
    {Ly  PrBody }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 12 }
    {Name SHORT }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 150 360 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Rect 0 0 150 360 }
   }
  }
