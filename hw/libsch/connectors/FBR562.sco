{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 1 1 0 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody PINNUM SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref RE? }
    {PPP 2 }
    {PinL
      ? 4 9
      ? 3 8
      ? 1 6
      ? 2 7
      ? 5 10
    }
    {Pkgs
      STD FBR562
    }
    {Lock 0 }
   {PPin : None
    {Pos -90 60 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 5 }
    {Name DOT }
   }
   {PPin : None
    {Pos 150 30 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 2 }
    {Name NORM }
   }
   {PPin : None
    {Pos 150 90 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 1 }
    {Name NORM }
   }
   {PPin : None
    {Pos -90 120 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 3 }
    {Name NORM }
   }
   {PPin : None
    {Pos -90 150 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 4 }
    {Name NORM }
   }
   {PLine : None
    {Pos 0 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 60 0 }
   }
   {PLine : None
    {Pos 45 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 12 -12 }
   }
   {PLine : None
    {Pos 57 48 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -24 0 }
   }
   {PLine : None
    {Pos 33 48 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 12 12 }
   }
   {PLine : None
    {Pos 45 48 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 -18 }
   }
   {PLine : None
    {Pos 45 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 15 0 }
   }
   {PLine : None
    {Pos 45 69 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 12 12 }
   }
   {PLine : None
    {Pos 57 81 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -24 0 }
   }
   {PLine : None
    {Pos 33 81 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 12 -12 }
   }
   {PLine : None
    {Pos 45 81 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 9 }
   }
   {PLine : None
    {Pos 45 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 15 0 }
   }
   {PLine : None
    {Pos 15 105 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 60 }
   }
   {PLine : None
    {Pos 15 165 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 15 0 }
   }
   {PLine : None
    {Pos 30 165 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 -60 }
   }
   {PLine : None
    {Pos 30 105 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -15 0 }
   }
   {PLine : None
    {Pos 0 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 36 0 }
   }
   {PLine : None
    {Pos 36 132 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -6 0 }
   }
   {PLine : None
    {Pos 15 132 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -6 0 }
   }
   {PLine : None
    {Pos 9 144 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 27 0 }
   }
   {PLine : None
    {Pos 36 156 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -6 0 }
   }
   {PLine : None
    {Pos 12 156 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 3 0 }
   }
   {PCir : None
    {Pos 36 126 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -6 -6 12 12 }
    {Angle 4319 1440 }
   }
   {PCir : None
    {Pos 36 126 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -6 -6 12 12 }
    {Angle 0 1440 }
   }
   {PCir : None
    {Pos 9 138 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -6 -6 12 12 }
    {Angle 2880 1439 }
   }
   {PCir : None
    {Pos 9 138 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -6 -6 12 12 }
    {Angle 1440 1440 }
   }
   {PCir : None
    {Pos 36 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -6 -6 12 12 }
    {Angle 4319 1440 }
   }
   {PCir : None
    {Pos 36 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -6 -6 12 12 }
    {Angle 0 1440 }
   }
   {PCir : None
    {Pos 0 156 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -6 -6 12 12 }
    {Angle 4319 1014 }
   }
   {PCir : None
    {Pos 12 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -6 -6 12 12 }
    {Angle 1440 1014 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 60 180 }
   }
  }
