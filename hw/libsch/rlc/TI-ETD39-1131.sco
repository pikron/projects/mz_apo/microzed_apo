{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 1 1 1 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody PINNUM SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref L? }
    {PPP 1 }
    {PinL
      ? 9
      ? 10
      ? 11
      ? 12
      ? 13
      ? 14
      ? 15
      ? 16
      ? 8
      ? 7
      ? 6
      ? 5
      ? 4
      ? 3
      ? 2
      ? 1
    }
    {Pkgs
    }
    {Lock 0 }
   {PPin : None
    {Pos -30 30 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 1 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 90 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 2 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 150 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 3 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 210 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 4 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 270 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 5 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 330 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 6 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 390 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 7 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 450 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 8 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 150 30 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 16 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 150 90 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 15 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 150 150 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 14 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 150 210 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 13 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 150 270 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 12 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 150 330 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 11 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 150 390 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 10 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 150 450 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 9 }
    {Name SHORT }
   }
   {PCir : None
    {Pos 30 105 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 4338 2861 }
   }
   {PCir : None
    {Pos 30 135 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 4338 2861 }
   }
   {PLine : None
    {Pos 0 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PLine : None
    {Pos 0 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PCir : None
    {Pos 15 141 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -3 -3 6 6 }
   }
   {PCir : None
    {Pos 30 225 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 4338 2861 }
   }
   {PCir : None
    {Pos 30 255 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 4338 2861 }
   }
   {PLine : None
    {Pos 0 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PLine : None
    {Pos 0 270 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PCir : None
    {Pos 15 261 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -3 -3 6 6 }
   }
   {PCir : None
    {Pos 30 405 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 4338 2861 }
   }
   {PCir : None
    {Pos 30 435 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 4338 2861 }
   }
   {PLine : None
    {Pos 0 390 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PLine : None
    {Pos 0 450 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PCir : None
    {Pos 15 441 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -3 -3 6 6 }
   }
   {PCir : None
    {Pos 90 105 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 1440 2861 }
   }
   {PCir : None
    {Pos 90 135 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 1440 2861 }
   }
   {PLine : None
    {Pos 90 90 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PLine : None
    {Pos 90 150 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PCir : None
    {Pos 105 99 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -3 -3 6 6 }
   }
   {PCir : None
    {Pos 90 285 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 1440 2861 }
   }
   {PCir : None
    {Pos 90 315 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 1440 2861 }
   }
   {PLine : None
    {Pos 90 270 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PLine : None
    {Pos 90 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PLine : None
    {Pos 120 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 60 }
   }
   {PLine : None
    {Pos 120 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 60 }
   }
   {PCir : None
    {Pos 105 321 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -3 -3 6 6 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 120 480 }
   }
  }
