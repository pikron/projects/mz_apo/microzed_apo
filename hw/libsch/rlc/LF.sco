{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 1 1 1 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody PINNUM SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref L? }
    {PPP 1 }
    {PinL
      ? 2
      ? 1
    }
    {Pkgs
    }
    {Lock 0 }
   {PPin : None
    {Pos 30 -30 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 1 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 30 90 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 2 }
    {Name SHORT }
   }
   {PCir : None
    {Pos 30 12 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
    {Angle 4319 1440 }
   }
   {PCir : None
    {Pos 30 12 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
    {Angle 0 1440 }
   }
   {PCir : None
    {Pos 30 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
    {Angle 4319 1440 }
   }
   {PCir : None
    {Pos 30 30 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
    {Angle 0 1440 }
   }
   {PCir : None
    {Pos 30 48 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
    {Angle 4319 1440 }
   }
   {PCir : None
    {Pos 30 48 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -9 -9 18 18 }
    {Angle 0 1440 }
   }
   {PLine : None
    {Pos 30 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 3 }
   }
   {PLine : None
    {Pos 30 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 -3 }
   }
   {PLine : None
    {Pos 42 3 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 54 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 60 60 }
   }
  }
