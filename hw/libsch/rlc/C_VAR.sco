{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 1 1 1 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody PINNUM SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref C? }
    {PPP 1 }
    {PinL
      ? 2
      ? 1
    }
    {Pkgs
    }
    {Lock 0 }
   {PPin : None
    {Pos 30 -30 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 1 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 30 90 }
    {Ly  PrBody PINNUM }
    {Rot 1 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 2 }
    {Name SHORT }
   }
   {PLine : None
    {Pos 6 24 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 48 0 }
   }
   {PLine : None
    {Pos 6 36 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 48 0 }
   }
   {PLine : None
    {Pos 9 51 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 42 -42 }
   }
   {PLine : None
    {Pos 6 36 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 6 }
   }
   {PLine : None
    {Pos 6 42 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 48 0 }
   }
   {PLine : None
    {Pos 54 42 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 -6 }
   }
   {PLine : None
    {Pos 54 24 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 -6 }
   }
   {PLine : None
    {Pos 54 18 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -48 0 }
   }
   {PLine : None
    {Pos 6 18 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 6 }
   }
   {PLine : None
    {Pos 30 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 18 }
   }
   {PLine : None
    {Pos 30 42 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 18 }
   }
   {PLine : None
    {Pos 45 3 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 12 12 }
   }
   {PLine : None
    {Pos 45 3 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 12 0 }
   }
   {PLine : None
    {Pos 57 3 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 12 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 60 60 }
   }
  }
