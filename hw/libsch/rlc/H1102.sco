{Layer
 {Ly PrBody
  {Col 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly PINNUM
  {Col 0 255 0 }
  {HLC 0 0 0 }
  {Stat 0 0 0 0  }
 }
 {Ly SIZEB 
  {Col 0 0 0 }
  {Stat 1 1 1 0  }
 }
}
{PSComp
 : None
    {Pos 0 0 }
    {Ly  PrBody PINNUM SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Val  }
    {Ref L? }
    {PPP 1 }
    {PinL
      ? 9
      ? 10
      ? 11
      ? 14
      ? 15
      ? 16
      ? 8
      ? 7
      ? 6
      ? 3
      ? 2
      ? 1
    }
    {Pkgs
      SMD1 H1102
    }
    {Lock 0 }
   {PPin : None
    {Pos -30 0 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 1 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 60 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 2 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 120 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 3 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 210 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 6 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 270 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 7 }
    {Name SHORT }
   }
   {PPin : None
    {Pos -30 330 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 0 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 8 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 150 0 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 16 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 150 60 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 15 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 150 120 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 14 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 150 210 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 11 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 150 270 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 10 }
    {Name SHORT }
   }
   {PPin : None
    {Pos 150 330 }
    {Ly  PrBody PINNUM }
    {Rot 0 }
    {Mir 1 }
    {Wd 1 }
    {Lock 1 }
    {PinNum 9 }
    {Name SHORT }
   }
   {PLine : None
    {Pos 0 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PLine : None
    {Pos 0 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PLine : None
    {Pos 0 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PLine : None
    {Pos 0 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PLine : None
    {Pos 0 270 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PLine : None
    {Pos 0 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 30 0 }
   }
   {PLine : None
    {Pos 120 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -30 0 }
   }
   {PLine : None
    {Pos 120 60 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -30 0 }
   }
   {PLine : None
    {Pos 120 120 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -30 0 }
   }
   {PLine : None
    {Pos 120 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -30 0 }
   }
   {PLine : None
    {Pos 120 270 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -30 0 }
   }
   {PLine : None
    {Pos 120 330 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 -30 0 }
   }
   {PLine : None
    {Pos 60 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 120 }
   }
   {PLine : None
    {Pos 63 0 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 120 }
   }
   {PLine : None
    {Pos 60 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 120 }
   }
   {PLine : None
    {Pos 63 210 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {XY  0 0 0 120 }
   }
   {PCir : None
    {Pos 30 15 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 4319 1440 }
   }
   {PCir : None
    {Pos 30 15 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 0 1440 }
   }
   {PCir : None
    {Pos 30 45 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 4319 1440 }
   }
   {PCir : None
    {Pos 30 45 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 0 1440 }
   }
   {PCir : None
    {Pos 30 75 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 4319 1440 }
   }
   {PCir : None
    {Pos 30 75 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 0 1440 }
   }
   {PCir : None
    {Pos 30 105 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 4319 1440 }
   }
   {PCir : None
    {Pos 30 105 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 0 1440 }
   }
   {PCir : None
    {Pos 90 15 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 2880 1439 }
   }
   {PCir : None
    {Pos 90 15 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 1440 1440 }
   }
   {PCir : None
    {Pos 90 45 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 2880 1439 }
   }
   {PCir : None
    {Pos 90 45 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 1440 1440 }
   }
   {PCir : None
    {Pos 90 75 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 2880 1439 }
   }
   {PCir : None
    {Pos 90 75 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 1440 1440 }
   }
   {PCir : None
    {Pos 90 105 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 2880 1439 }
   }
   {PCir : None
    {Pos 90 105 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 1440 1440 }
   }
   {PCir : None
    {Pos 30 225 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 4319 1440 }
   }
   {PCir : None
    {Pos 30 225 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 0 1440 }
   }
   {PCir : None
    {Pos 30 255 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 4319 1440 }
   }
   {PCir : None
    {Pos 30 255 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 0 1440 }
   }
   {PCir : None
    {Pos 30 285 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 4319 1440 }
   }
   {PCir : None
    {Pos 30 285 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 0 1440 }
   }
   {PCir : None
    {Pos 30 315 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 4319 1440 }
   }
   {PCir : None
    {Pos 30 315 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 0 1440 }
   }
   {PCir : None
    {Pos 90 225 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 2880 1439 }
   }
   {PCir : None
    {Pos 90 225 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 1440 1440 }
   }
   {PCir : None
    {Pos 90 255 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 2880 1439 }
   }
   {PCir : None
    {Pos 90 255 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 1440 1440 }
   }
   {PCir : None
    {Pos 90 285 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 2880 1439 }
   }
   {PCir : None
    {Pos 90 285 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 1440 1440 }
   }
   {PCir : None
    {Pos 90 315 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 2880 1439 }
   }
   {PCir : None
    {Pos 90 315 }
    {Ly  PrBody }
    {Rot 0 }
    {Mir 0 }
    {Wd 2 }
    {Circ -15 -15 30 30 }
    {Angle 1440 1440 }
   }
   {PRect : None
    {Pos 0 0 }
    {Ly  SIZEB  }
    {Rot 0 }
    {Mir 0 }
    {Wd 0 }
    {Rect 0 0 120 330 }
   }
  }
