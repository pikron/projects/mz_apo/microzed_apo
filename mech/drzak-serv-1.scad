
x=1;
d=50;
$fn=72;

difference(){
    union(){
        translate([-14,22,0]) cube([140,20,5]);
        translate([0,0,0]) cube([12,30,5]);
        translate([95,0,0]) cube([12,30,5]);
    }
    translate([6,6,-0.1]) cylinder(d=3.5,h=6);
    translate([101,6,-0.1]) cylinder(d=3.5,h=6);
    translate([x,32,0]) servo();
    translate([x+d+3,32,0]) servo();
    translate([x+d+d,32,0]) servo();
}



module servo()
{
    translate([-23.6/2+5,-6.5,-0.1]) cube([23.6,13,10]);
    translate([-23.6/2+5,-6.5,1.8]) rotate([0,-25,0])cube([2,13,10]);
    translate([23.6/2+3,-6.5,1.8]) rotate([0,25,0])cube([2,13,10]);
    translate([-27.5/2+5,0,-0.1]) cylinder(d=1.5,h=10);
    translate([27.5/2+5,0,-0.1]) cylinder(d=1.5,h=10);
    translate([-34/2+5,-7,3]) cube([34,14,10]);
}