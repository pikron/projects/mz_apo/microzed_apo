
//L=216;
//L=210;
L=228;

h=1.8;
$fn=150;
n=round(L/3);
L1=n*3;

echo("L1 ",L1);
echo("n ",n);
D=L/PI;

difference(){
   cylinder(d=D+2,h=h);
   translate([0,0,-0.1])cylinder(d=D,h=h+0.2); 
}
for(i=[0:360/n:359])
    rotate([0,0,i]){
        translate([D/2-0.3,0,0]) cylinder(d=1.6,h=h);
        translate([D/2-0.3,-1.6/2,0]) cube([1,1.6,h]);
    }