
x=14.5;
d=36.2;
$fn=72;

difference(){
    union(){
        translate([0,10,0]) cube([107,29,2]);
        translate([0,0,0]) cube([12,12,4]);
        translate([95,0,0]) cube([12,12,4]);
        translate([0,10,0]) cube([107,2,4]);
        translate([0,38,0]) cube([107,1,4]);
        translate([0,0,0]) cube([1,38,4]);
        translate([x+d+d/2+4.5,10,0]) cube([1,28,4]);
        translate([x+d/2+4.5,10,0]) cube([1,28,4]);
    }
    translate([6,6,-0.1]) cylinder(d=3.5,h=5);
    translate([101,6,-0.1]) cylinder(d=3.5,h=5);
    translate([x,30,0]) servo();
    translate([x+d,30,0]) servo();
    translate([x+d+d,30,0]) servo();
}


module servo()
{
    translate([-6.6,-6.5,-0.1]) cube([23,13,10]);
    translate([-27.5/2+5,0,-0.1]) cylinder(d=1.5,h=10);
    translate([27.5/2+5,0,-0.1]) cylinder(d=1.5,h=10);
}