D=19;
D1=17;
h=4;
$fn=90;

include <gear/gear.scad>

difference(){
    union(){
        cylinder(d=D,h=0.3);
        translate([0,0,0.3]) cone(D,D1,0.6);
        cylinder(d=D1,h=h);
        translate([0,0,h-0.9]) cone(D1,D,0.6);
        translate([0,0,h-0.3]) cylinder(d=D,h=0.3);
        cylinder(d=16.4,h=6);
    }
    translate([0,0,-0.1])  cylinder(d=11.6,h=6);
    translate([0,0,1])  cylinder(d=14.8,h=6);
    translate([5.6,-1,-0.3]) rotate([0,0,45]) cube([1.4,1.4,6]);
}
//linear_extrude(height=3,scale=0.9)
//gear2D(number_of_teeth=14,circular_pitch=3.8,pressure_angle=30,
 //    depth_ratio=0.5,clearance=0);
 
 module cone(d1,d2,h){
    linear_extrude(height = h,convexity = 10,scale=d2/d1)
    circle(d = d1);
}
