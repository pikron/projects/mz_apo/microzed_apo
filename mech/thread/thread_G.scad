

module thread_out_pitch(dia,hi,p,thr=$fn,st=3)
// make an outside thread (as used on a bolt) with supplied pitch
//  dia=diameter, 6=M6 etc
//  hi=height, 10=make a 10mm long thread
//  p=pitch
//  thr=thread quality, 10=make a thread with 10 segments per turn
{
	h=((p/2)/tan(55/2))/18;
	Rmin=(dia/2)-(12*h);	
	s=360/thr;				// length of segment in degrees
	n=round((hi-5*p/6)/(p/thr));			// number of segments
	for(sg=[0:st-1])
		th_out_pt1(Rmin,p,s,sg,thr,h*11*(sg+1)/(st+1),h*11*(sg+2)/(st+1),p/thr);
	for(sg=[st:n-st])
		th_out_pt1(Rmin,p,s,sg,thr,h*11,h*11,p/thr);
	for(sg=[n-st+1:n])
		th_out_pt1(Rmin,p,s,sg,thr,h*11*(n-sg+2)/(st+1),h*11*(n-sg+1)/(st+1),p/thr);
}


module thread_in_pitch(dia,hi,p,thr=$fn,st=3)
// make an inside thread (as used on a nut)
//  dia = diameter, 6=M6 etc
//  hi = height, 10=make a 10mm long thread
//  p=pitch
//  thr = thread quality, 10=make a thread with 10 segments per turn
{
	h=((p/2)/tan(55/2))/18;
	Rmin=(dia/2)-(11*h);	
    echo("Dmin=", Rmin*2);
	s=360/thr;				// length of segment in degrees
	n=round((hi-5*p/6)/(p/thr));			// number of segments
	for(sg=[0:st-1])
		th_in_pt1(dia/2,p,s,sg,thr,h*11*(sg+1)/(st+1),h*11*(sg+2)/(st+1),p/thr);
	for(sg=[st:n-st+1])
		th_in_pt1(dia/2,p,s,sg,thr,h*11,h*11,p/thr);
	for(sg=[n-st+1:n])
		th_in_pt1(dia/2,p,s,sg,thr,h*11*(n-sg+2)/(st+1),h*11*(n-sg+1)/(st+1),p/thr);
}


module th_out_pt1(rt,p,s,sg,thr,h1,h2,sh)
// make a part of an outside thread (single segment)
//  rt = radius of thread (nearest centre)
//  p = pitch
//  s = segment length (degrees)
//  sg = segment number
//  thr = segments in circumference
//  h = ISO h of thread / 8
//  sh = segment height (z)
{
	as = (sg % thr) * s;			// angle to start of seg
	ae = as + s;// - (s/10);		// angle to end of seg (with overlap)
	z = sh*sg;
	//pp = p/2;
	//   1,5
	//   |\
	//   | \ 2,6
	//   |  |
	//   |  |3,7
    //   | / 
	//   |/
	//   0,4
	//  view from front (x & z) extruded in y by sg
	//  
	//echo(str("as=",as,", ae=",ae," z=",z));
	polyhedron(
		points = [
			[cos(as)*rt,sin(as)*rt,z],								// 0
			[cos(as)*rt,sin(as)*rt,z+(5/6*p)],						// 1
			[cos(as)*(rt+h1),sin(as)*(rt+h1),z+(10/18*p)],		// 2
			[cos(as)*(rt+h1),sin(as)*(rt+h1),z+(5/18*p)],		// 3
			[cos(ae)*rt,sin(ae)*rt,z+sh],							// 4
			[cos(ae)*rt,sin(ae)*rt,z+(5/6*p)+sh],					// 5
			[cos(ae)*(rt+h2),sin(ae)*(rt+h2),z+sh+(10/18*p)],	// 6
			[cos(ae)*(rt+h2),sin(ae)*(rt+h2),z+sh+(5/18*p)]],	// 7
		faces = [
            [0,1,2],[0,2,3],  // bottom
            [4,5,1],[4,1,0],  // front
            [7,6,5],[7,5,4],  // top
            [5,6,2],[5,2,1],  // right
            [6,7,3],[6,3,2],  // back
            [3,4,0],[4,3,7]]); // left

}

module th_in_pt1(rt,p,s,sg,thr,h1,h2,sh)
// make a part of an inside thread (single segment)
//  rt = radius of thread (nearest centre)
//  p = pitch
//  s = segment length (degrees)
//  sg = segment number
//  thr = segments in circumference
//  h = ISO h of thread / 8
//  sh = segment height (z)
{
	as = ((sg % thr) * s - 180);	// angle to start of seg
	ae = as + s ;		// angle to end of seg (with overlap)
	z = sh*sg;
	pp = p/2;
	//         3,7
	//          /|
	//     2,6 / | 
	//        |  | 
	//     1,5|  | 
 	//         \ |
	//          \|
	//         0,4
	//  view from front (x & z) extruded in y by sg
	//  
	polyhedron(
		points = [
			[cos(as)*(rt),sin(as)*(rt),z],				//0
			[cos(as)*(rt-h1),sin(as)*(rt-h1),z+(5/18*p)],						//1
			[cos(as)*(rt-h1),sin(as)*(rt-h1),z+(10/18*p)],						//2
			[cos(as)*(rt),sin(as)*(rt),z+(5/6*p)],		//3
			[cos(ae)*(rt),sin(ae)*(rt),z+sh],			//4
			[cos(ae)*(rt-h2),sin(ae)*(rt-h2),z+(5/18*p)+sh],					//5
			[cos(ae)*(rt-h2),sin(ae)*(rt-h2),z+(10/18*p)+sh],					//6
			[cos(ae)*(rt),sin(ae)*(rt),z+(5/6*p)+sh]],  	//7
		faces = [
            [0,1,2],[0,2,3],  // bottom
            [4,5,1],[4,1,0],  // front
            [7,6,5],[7,5,4],  // top
            [5,6,2],[5,2,1],  // right
            [6,7,3],[6,3,2],  // back
            [7,4,0],[7,0,3]]); // left
}
