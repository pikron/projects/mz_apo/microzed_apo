$fn=36;

include <thread/thread_M.scad>

translate([0,0,0]) union(){
    thread_in_pitch(3.15,14.5,0.5);
    difference(){
        cylinder(d=(5.5/cos(30)),h=14.5,$fn=6);
        translate([0,0,-0.1]) cylinder(d=3.15,h=15.2);
    }
}

