
D=40;
D1=38;
h=5;
$fn=90;

difference(){
    union(){
        cylinder(d=D,h=0.8);
        translate([0,0,0.8]) cone(D,D1,0.4);
        cylinder(d=D1,h=h);
        translate([0,0,h-1.2]) cone(D1,D,0.4);
        translate([0,0,h-0.8]) cylinder(d=D,h=0.8);
    }
    translate([0,0,-0.1])cylinder(d=2.5,h=h);
    translate([0,0,2.8]) cone(D1-5,D1-1,3.6);
    translate([0,0,0.7]) cylinder(d=5.08,h=2.3,$fn=21);
    for(i=[0:7]){
        rotate([0,0,i*360/8]) translate([13,0,-0.1]) cylinder(d=8.4,h=6);
//        rotate([0,0,i*360/8+22.5]) translate([6.5,0,-0.1]) cylinder(d=4,h=6);
    }    
    for(i=[0:4])
        rotate([0,0,i*360/4+22.5]) translate([6.2,0,-0.1]) 
            cylinder(d=5.5,h=6);
 
    translate([12,-0.35,1.2]) cube([10,0.7,5]);
    translate([12,-1.4,5.7]) rotate([-45,0,0]) cube([10,2,2]);
}


for(i=[0:20])
    rotate([0,0,i*360/21]) translate([-2.7,-0.5,0.3]) 
        rotate([0,0,45]) cube([0.7,0.7,2.5]);

module cone(d1,d2,h){
    linear_extrude(height = h,convexity = 10,scale=d2/d1)
    circle(d = d1);
}
